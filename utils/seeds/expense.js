/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('expense').del()
  await knex('expense').insert([
    { account_code: '2-002', amount: '2000000' },
    { account_code: '2-003', amount: '1000000' }
  ])
}
