/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> } 
 */
exports.seed = async function(knex) {
  // Deletes ALL existing entries
  await knex('income').del()
  await knex('income').insert([
    { account_code: '1-001', amount: '10000000' }
  ]);
};
