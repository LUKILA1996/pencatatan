/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.seed = async function (knex) {
  // Deletes ALL existing entries
  await knex('account').del()
  await knex('account').insert([
    { account_code: '1-001', account_name: 'Modal' },
    { account_code: '1-002', account_name: 'Sepatu' },
    { account_code: '1-003', account_name: 'Kaos' },
    { account_code: '1-004', account_name: 'Jaket' },
    { account_code: '1-005', account_name: 'Celana' },
    { account_code: '2-001', account_name: 'modal' },
    { account_code: '2-002', account_name: 'Sepatu' },
    { account_code: '2-003', account_name: 'Kaos' },
    { account_code: '2-004', account_name: 'Jaket' },
    { account_code: '2-005', account_name: 'Celana' },
  ])
}
