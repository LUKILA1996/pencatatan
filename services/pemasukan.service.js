const db = require('../utils/db')

const getPemasukan = async (req, res) => {
    const pemasukan = await db('income').select('*')

    return res.status(200).json({data:pemasukan})
}

module.exports = {getPemasukan}