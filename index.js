const express = require('express')
const app = express()

app.use(express.json())
const router = require('./routes')

app.use(router)

app.listen(3000, () => {
  console.log(`Buku warung running at port 3000`)
})
