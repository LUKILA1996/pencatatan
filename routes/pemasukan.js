const router = require('express').Router()
const pemasukanService = require('../services/pemasukan.service')

router.get('/', pemasukanService.getPemasukan)

module.exports = router
