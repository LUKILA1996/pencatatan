const router = require('express').Router()
const pengeluaranRouter = require('./pengeluaran')
const pemasukanRouter = require('./pemasukan')
const accountRouter = require('./account')
const rootRouter = require('./root')

router.use('/', rootRouter)
router.use('/account', accountRouter)
router.use('/pengeluaran', pengeluaranRouter)
router.use('/pemasukan', pemasukanRouter)

module.exports = router
