const router = require('express').Router()
const rootService = require('../services/root.service')

router.get('/', rootService.get)

module.exports = router
